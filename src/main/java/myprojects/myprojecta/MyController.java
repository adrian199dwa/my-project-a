package myprojects.myprojecta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class MyController {

    @Autowired
    PersonRepository rep;

    @GetMapping("/")
    public String informUser() {
        return "/person?id=x or /people?last=x";
    }

    @GetMapping("/person")
    public Optional<Person> personInfo(@RequestParam Long id) {
        return rep.findById(id);
    }

    @GetMapping("/people")
    public List<Person> peopleInfo(@RequestParam String last) {
        return rep.findByLastname(last);
    }

    @PostMapping("/personIn")
    public void insertPerson(@RequestParam String first, @RequestParam String last) {
        rep.save(new Person(first, last));
    }

    @PostMapping("/personOut")
    public void deletePerson(@RequestParam Long id) {
        rep.deleteById(id);
    }
}
