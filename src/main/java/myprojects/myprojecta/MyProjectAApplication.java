package myprojects.myprojecta;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MyProjectAApplication {

	private static final Logger log = LoggerFactory.getLogger(MyProjectAApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(MyProjectAApplication.class, args);
	}

	@Bean
	public CommandLineRunner runner(PersonRepository rep) {
		return args -> {
			log.info("Just an info.");
			for (Person person : rep.findAll()) {
				log.info(person.toString());
			}
		};
	}

}
